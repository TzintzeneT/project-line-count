# Project line count

**count the length of all files inside of a directory**

## Installation:

**install by executing the following command(s):**

```bash
curl https://gitlab.com/TzintzeneT/project-line-count/-/raw/main/lineCount.sh -o plc &&
  chmod +x plc &&
  sudo mv plc /usr/local/bin/
```

**to uninstall you can use:**

```bash
sudo rm /usr/local/bin/plc
```

**to update:**
execute the [installation script](#installation) again

## Usage:

#### syntax:

```bash
plc <directory> [flags] 
```

#### available flags:

- `-r` count recursively, including files in sub-directories.
- `-n` nerd stats, will print more data (won't work if -c is used)
- `-c` clean output, print only the result with no details (useful when combining with other scripts & commands)

#### example:

```bash
plc ~/Documents/code -r
```

this configuration will print the length of all files in the directory "code" & it's sub-directories

**note:** if you won't specify a directory `plc` will default to the current working directory ([`pwd`](https://water.phila.gov/), a.k.a. Philadelphia Water Department)

## License

this program is licensed under the awesome GNU GPLv3, see LICENSE for more information.