#!/bin/bash

# count all files
count=0
# project directory
projectDir=$1
# options flags
flags=$2

# if no directory specified
if [[ $1 == "" || $1 == "-"* ]]; then
  # count current directory
	projectDir=$PWD

	# if there are flags
	if [[ $1 == "-"* ]]; then
	  # save them
	  flags=$1
	fi

fi


function countFilesInCurrentDir() {

  # if not asking for clean output & asking for nerd stats
  if [[ $flags != *"c"* ]]; then

    # cut the current dir in the project prom the full path
    currentDir=${PWD#*${projectRootDir}}
    # add the project to the start of the path
    currentDir="$projectRootDir$currentDir:"

    # print progression
    printf "$currentDir"
  fi

  # counter for the directory
  directoryCounter=0

  # for each file in the directory
  for file in ./*.*
  	do

      # if there are files
  	  if [[ $file != "./*.*" ]]; then

        # if it is a text file
  	    if [[ $(file --mime-type $file) == *"text"* ]]; then

          # if the file is not empty
          if [[ $(cat $file) != "" ]]; then

            # add it's length to the count
            num=$(cat $file | wc -l)
            directoryCounter=$((directoryCounter + num + 1))
            count=$((count + num + 1))

            # if not asking for clean output & asking for nerd stats
            if [[ $flags != *"c"* && $flags == *"n"* ]]; then

              # print file details
              printf "\n  $file: $((num + 1)) lines"
            fi
          fi
        fi
  	  fi
  done

  # if not asking for clean output & not asking nerd stats
  if [[ $flags != *"c"* && $flags != *"n"* ]]; then

    # print the directory counter
    printf " $directoryCounter lines\n"
  fi

  # if not asking for clean output & asking for nerd stats
  if [[ $flags != *"c"* && $flags == *"n"* ]]; then

    # print file details
    printf "\ndirectory sum: $directoryCounter\n\n"
  fi
}


function goOverDirectories() {

  countFilesInCurrentDir

  # if doing a recursive count
  if [[ $flags == *"r"* ]]; then

      # for each sub-directory of the current directory
      for d in */ ; do

          # if there are sub-directories
          if [[ $d != "*/" ]]; then

              # cd into that directory & do a recursive call
              cd $d
              goOverDirectories
              cd ..
          fi
      done
  fi
}

# cd in to the project directory
cd $projectDir
# current directory
projectRootDir=${PWD##*/}

goOverDirectories

# if not asking for clean output & not asking nerd stats
if [[ $flags != *"c"* && $flags != *"n"* ]]; then
      echo ""
fi

# if not asking for clean output
if [[ $flags != *"c"* ]]; then
    echo "length of project: $count lines"
else
  # print result
  echo $count
fi

